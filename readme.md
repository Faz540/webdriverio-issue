Author: Paul Farrell

## Clone Repo:
```sh
git clone https://bitbucket.org/Faz540/webdriverio-issue/
```

## Install Dependencies:
```sh
yarn install
```

## Run Test:
```sh
yarn test
```